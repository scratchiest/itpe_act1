import React, { Component } from 'react';
import { StyleSheet, View, Button, ScrollView } from 'react-native';

export default class HomeScreen extends Component {
  render() {
    return (
      <ScrollView>
        <View style={[styles.container, styles.spacing]}>
          <Button title="Go to Details" onPress={() => this.props.navigation.navigate('Details')} />
          <Button title="Go to ActivityIndicator" onPress={() => this.props.navigation.navigate('ActivityIndicator')} />
          <Button title="Go to Button" onPress={() => this.props.navigation.navigate('Button')} />
          <Button title="Go to DrawerLayoutAndroid" onPress={() => this.props.navigation.navigate('DLA')} />
          <Button title="Go to Image" onPress={() => this.props.navigation.navigate('Image')} />
          <Button title="Go to KeyboardAvoidingView" onPress={() => this.props.navigation.navigate('KAV')} />
          <Button title="Go to ListView" onPress={() => this.props.navigation.navigate('ListView')} />
          <Button title="Go to Modal" onPress={() => this.props.navigation.navigate('Modal')} />
          <Button title="Go to Picker" onPress={() => this.props.navigation.navigate('Picker')} />
          <Button title="Go to ProgressBarAndroid" onPress={() => this.props.navigation.navigate('PBA')} />
          <Button title="Go to RefreshControl" onPress={() => this.props.navigation.navigate('RefreshControl')} />
          <Button title="Go to ScrollView" onPress={() => this.props.navigation.navigate('ScrollView')} />
          <Button title="Go to SectionList" onPress={() => this.props.navigation.navigate('SectionList')} />
          <Button title="Go to Slider" onPress={() => this.props.navigation.navigate('Slider')} />
          <Button title="Go to StatusBar" onPress={() => this.props.navigation.navigate('StatusBar')} />
          <Button title="Go to Switch" onPress={() => this.props.navigation.navigate('Switch')} />
          <Button title="Go to Text" onPress={() => this.props.navigation.navigate('Text')} />
          <Button title="Go to TextInput" onPress={() => this.props.navigation.navigate('TextInput')} />
          <Button title="Go to TouchableHighlight" onPress={() => this.props.navigation.navigate('TH')} />
          <Button title="Go to TouchableNativeFeedback" onPress={() => this.props.navigation.navigate('TNF')} />
          <Button title="Go to TouchableOpacity" onPress={() => this.props.navigation.navigate('TO')} />
          <Button title="Go to TouchableWithoutFeedback" onPress={() => this.props.navigation.navigate('TWF')} />
          <Button title="Go to View" onPress={() => this.props.navigation.navigate('View')} />
          <Button title="Go to ViewPagerAndroid" onPress={() => this.props.navigation.navigate('VPA')} />
          <Button title="Go to WebView" onPress={() => this.props.navigation.navigate('WebView')} />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  spacing: {
    padding: 20
  },
});
